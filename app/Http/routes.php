<?php

use App\Website;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

$app->get('/', function() use ($app) {
    return view('index');
});

$app->get('dashboard', function () {
    return view('dashboard');
});

$app->get('api/website', function (Request $request) use ($app) {
    $name = $request->input('name');

    if (strlen($name) < 1) {
        return new Response('', 403);
    }

    $website = Website::where('name', '=', $name)->first();

    if (empty($website)) {
        return new Response('', 404);
    }

    return json_encode([
        'name' => $website->name,
        'user' => $website->user,
        'repo' => $website->repo,
    ]);
});

$app->post('api/website', function (Request $request) use ($app) {
    # Check if the name already exists.
    $nameExists = Website::where('name', '=', $request->get('website'))->first();
    if ($nameExists) {
        return redirect()->back();
    }

    # Check if the input's good.
    if (
        strlen($request->get('website')) < 1 ||
        strlen($request->get('repo')) < 1 ||
        strlen($request->get('user')) < 1 ||
        strlen($request->get('password')) < 1
    ) {
        return redirect()->back();
    }

    $website = Website::create([
        'name' => $request->get('website'),
        'password' => Hash::make($request->get('password')),
        'repo' => $request->get('repo'),
        'user' => $request->get('user'),
    ]);

    $cmd = getenv('CREATE_CMD');
    $cmd = str_replace('$1', $website->name . 'x.iili.li', $cmd);
    $cmd = str_replace('$2', $website->user, $cmd);
    $cmd = str_replace('$3', $website->repo, $cmd);
    exec($cmd);

    return redirect()->to('http://' . $website->name . 'x.iili.li');
});

$app->post('api/website/update', function (Request $request) use ($app) {
    # Check if the name exists.
    $website = Website::where('name', '=', $request->get('website'))->first();
    if (empty($website)) {
        return redirect()->back();
    }

    if (!Hash::check($request->get('password'), $website->password)) {
        return redirect()->back();
    }

    $website->update([
        'repo' => $request->get('repo'),
        'user' => $request->get('user'),
    ]);

    $cmd = getenv('CREATE_CMD');
    $cmd = str_replace('$1', $request->get('website') . 'x.iili.li', $cmd);
    $cmd = str_replace('$2', $request->get('user'), $cmd);
    $cmd = str_replace('$3', $request->get('repo'), $cmd);
    exec($cmd);

    return redirect()->to('http://' . $website->name . 'x.iili.li');
});
