var updateRetrieve = function (el) {
  var website = $('#update-website').val();

  if (website.length < 1) {
    return;
  }

  $.get('/api/website/?name=' + website)
    .success(function (data) {
      data = $.parseJSON(data);
      $('#update-website').val(data.name);
      $('#update-user').val(data.user);
      $('#update-repo').val(data.repo);
      $('#update').removeClass('hidden').addClass('show');
    });
};

$('#update-retrieve').click(function () {
  updateRetrieve();
});
